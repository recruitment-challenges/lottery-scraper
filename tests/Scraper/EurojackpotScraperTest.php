<?php

namespace LotteryScraper\Tests\Scraper;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use LotteryScraper\Scraper\EurojackpotScraper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DomCrawler\Crawler;

final class EurojackpotScraperTest extends TestCase
{
    /**
     * @var EurojackpotScraper scraper under test
     */
    private $scraper;

    /**
     * @var MockHandler handler for mocked responses
     */
    private $mockHandler;

    protected function setUp()
    {
        $this->mockHandler = new MockHandler();

        $this->scraper = new EurojackpotScraper(
            new HttpClient([
                'handler' => HandlerStack::create($this->mockHandler),
            ]),
            new Crawler()
        );
    }

    public function testOkResponceWithCorrectData()
    {
        $body = file_get_contents(realpath(__DIR__.'//..//samples//eurojackpot_200_correct.html'));

        $this->mockHandler->append(
            new Response(200, [], $body)
        );

        $result = $this->scraper->scrape();

        $this->assertSame(
            [
                'Eurojackpot' => [
                    'standardNumbers' => ['1', '2', '3', '4', '5'],
                    'extraNumbers' => ['6', '7'],
                    'drawId' => '111',
                    'drawDate' => '21-12-18, piątek',
                ],
            ],
            $result
        );
    }

    /**
     * @expectedException \LotteryScraper\Exception\NoResultException
     */
    public function testOkResponceWithIncorrectData()
    {
        $body = file_get_contents(realpath(__DIR__.'//..//samples//eurojackpot_200_incorrect.html'));

        $this->mockHandler->append(
            new Response(200, [], $body)
        );

        $this->scraper->scrape();
    }

    /**
     * @expectedException \LotteryScraper\Exception\NoResultException
     * @testWith    [500]
     *              [400]
     *              [300]
     */
    public function testBadResponce(int $errorCode)
    {
        $this->mockHandler->append(new Response($errorCode));

        $this->scraper->scrape();
    }
}
