<?php

namespace LotteryScraper\Tests\Scraper;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use LotteryScraper\Scraper\EuroMilionsScraper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DomCrawler\Crawler;

final class EuroMilionsScraperTest extends TestCase
{
    /**
     * @var EuroMilionsScraper scraper under test
     */
    private $scraper;

    /**
     * @var MockHandler handler for mocked responses
     */
    private $mockHandler;

    protected function setUp()
    {
        $this->mockHandler = new MockHandler();

        $this->scraper = new EuroMilionsScraper(
            new HttpClient([
                'handler' => HandlerStack::create($this->mockHandler),
            ]),
            new Crawler()
        );
    }

    public function testOkResponceWithCorrectData()
    {
        $body = file_get_contents(realpath(__DIR__.'//..//samples//euromilions_200_correct.html'));

        $this->mockHandler->append(
            new Response(200, [], $body)
        );

        $result = $this->scraper->scrape();

        $this->assertSame(
            [
                'EuroMilions' => [
                    'standardNumbers' => ['2', '12', '15', '34', '50'],
                    'extraNumbers' => ['3', '4'],
                    'drawDate' => 'Friday, 21 December 2018',
                ],
            ],
            $result
        );
    }

    /**
     * @expectedException \LotteryScraper\Exception\NoResultException
     */
    public function testOkResponceWithIncorrectData()
    {
        $body = file_get_contents(realpath(__DIR__.'//..//samples//euromilions_200_incorrect.html'));

        $this->mockHandler->append(
            new Response(200, [], $body)
        );

        $this->scraper->scrape();
    }

    /**
     * @expectedException \LotteryScraper\Exception\NoResultException
     * @testWith    [500]
     *              [400]
     *              [300]
     */
    public function testBadResponce(int $errorCode)
    {
        $this->mockHandler->append(new Response($errorCode));

        $this->scraper->scrape();
    }
}
