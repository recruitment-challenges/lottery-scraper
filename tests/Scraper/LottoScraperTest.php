<?php

namespace LotteryScraper\Tests\Scraper;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use LotteryScraper\Scraper\LottoScraper;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DomCrawler\Crawler;

final class LottoScraperTest extends TestCase
{
    /**
     * @var LottoScraper scraper under test
     */
    private $scraper;

    /**
     * @var MockHandler handler for mocked responses
     */
    private $mockHandler;

    protected function setUp()
    {
        $this->mockHandler = new MockHandler();

        $this->scraper = new LottoScraper(
            new HttpClient([
                'handler' => HandlerStack::create($this->mockHandler),
            ]),
            new Crawler()
        );
    }

    public function testOkResponceWithCorrectData()
    {
        $body = file_get_contents(realpath(__DIR__.'//..//samples//lotto_200_correct.html'));

        $this->mockHandler->append(
            new Response(200, [], $body)
        );

        $result = $this->scraper->scrape();

        $this->assertSame(
            [
                'Lotto' => [
                    'drawId' => '1234',
                    'drawDate' => '21-11-15, czwartek',
                    'standardNumbers' => ['1', '4', '2', '5', '3', '6'],
                ],
                'Lotto Plus' => [
                    'drawId' => '2143',
                    'drawDate' => '20-11-15, środa',
                    'standardNumbers' => ['11', '14', '12', '15', '13', '16'],
                ],
                'Super Szansa' => [
                    'drawId' => '4321',
                    'drawDate' => '19-11-15, wtorek',
                    'standardNumbers' => ['21', '25', '22', '26', '23', '27', '24'],
                ],
            ],
            $result
        );
    }

    /**
     * @expectedException \LotteryScraper\Exception\NoResultException
     */
    public function testOkResponceWithIncorrectData()
    {
        $body = file_get_contents(realpath(__DIR__.'//..//samples//lotto_200_incorrect.html'));

        $this->mockHandler->append(
            new Response(200, [], $body)
        );

        $this->scraper->scrape();
    }

    /**
     * @expectedException \LotteryScraper\Exception\NoResultException
     * @testWith    [500]
     *              [400]
     *              [300]
     */
    public function testBadResponce(int $errorCode)
    {
        $this->mockHandler->append(new Response($errorCode));

        $this->scraper->scrape();
    }
}
