<?php

namespace LotteryScraper\Tests\Command;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use LotteryScraper\Command\ScrapeCommand;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

final class ScrapeCommandTest extends TestCase
{
    use \phpmock\phpunit\PHPMock;

    /**
     * @var CommandTester
     */
    private $commandTester;

    /**
     * @var MockHandler handler for mocked responses
     */
    private $mockHandler;

    protected function setUp()
    {
        $this->mockHandler = new MockHandler();

        $application = new Application();

        $command = $application->add(
            new ScrapeCommand(
                new HttpClient([
                    'handler' => HandlerStack::create($this->mockHandler),
                ])
            )
        );

        $this->commandTester = new CommandTester($command);
    }

    public function testSuccessfulCommandExecution()
    {
        $filePutContents = $this->getFunctionMock('LotteryScraper\Command', 'file_put_contents');
        $filePutContents->expects($this->once())->willReturn(100);

        // order of elements needs to be aligned with ScrapeCommand::SCRAPERS
        $filePrefixes = ['eurojackpot', 'euromilions', 'lotto'];

        foreach ($filePrefixes as $prefix) {
            $body = file_get_contents(realpath(__DIR__."//..//samples//${prefix}_200_correct.html"));
            $this->mockHandler->append(
                new Response(200, [], $body)
            );
        }

        $this->commandTester->execute([]);

        $this->assertSame("Scraping lottery results sucessfuly completed!\r\n", $this->commandTester->getDisplay());
    }

    public function testCommandExecutionWithError()
    {
        $this->mockHandler->append(new Response(500));

        $this->commandTester->execute([]);

        $this->assertSame("Can not process expected data\r\n", $this->commandTester->getDisplay());
    }

    public function testCommandExecutionWithFailOnSaveReport()
    {
        $filePutContents = $this->getFunctionMock('LotteryScraper\Command', 'file_put_contents');
        $filePutContents->expects($this->once())->willReturn(false);

        // order of elements needs to be aligned with ScrapeCommand::SCRAPERS
        $filePrefixes = ['eurojackpot', 'euromilions', 'lotto'];

        foreach ($filePrefixes as $prefix) {
            $body = file_get_contents(realpath(__DIR__."//..//samples//${prefix}_200_correct.html"));
            $this->mockHandler->append(
                new Response(200, [], $body)
            );
        }

        $this->commandTester->execute([]);

        $this->assertSame(
            'Can not save report to '.getcwd()."//lotery_result.json\r\n",
            $this->commandTester->getDisplay()
        );
    }
}
