# Lottery Scraper

This application is designed to scrap lottery results from lottery organizers
web pages.

It has 3 integrated scrapers for:
* Eurojackpot
* Euromilions
* Lotto (Lotto, Lotto Plus, Super Szansa)

# Instalation

Clone repository:
```sh
git clone git@gitlab.com:recruitment-challenges/lottery-scraper.git
```
then run [Composer](https://getcomposer.org/):
```sh
cd lottery-scraper
composer install
```

# Usage

To save the newest results:
```sh
php app.php
```

Results are saved to file **lotery_result.json** in application directory.

# Tests

100% test coverage is achieved. To check coverage report:
```sh
bin/phpunit
```
Report is stored in **reports/coverage**.

Coverage report will be generate only when [XDebug is installed](https://xdebug.org/wizard.php).
