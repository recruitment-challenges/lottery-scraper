<?php

namespace LotteryScraper\Exception;

/**
 * Exception used when a scraper can't scrap expected data.
 */
class NoResultException extends \Exception
{
}
