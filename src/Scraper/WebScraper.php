<?php

namespace LotteryScraper\Scraper;

use GuzzleHttp\ClientInterface as HttpClientInterface;
use LotteryScraper\Exception\NoResultException;
use Symfony\Component\DomCrawler\Crawler;

abstract class WebScraper
{
    /**
     * @var string URL path to what has to be scraped
     */
    const URL = '';

    /**
     * @var Crawler crawler used to travers the DOM
     */
    protected $domCrawler;

    /**
     * @var HttpClientInterface client used to make http requests
     */
    private $httpClient;

    /**
     * @param HttpClientInterface $client http client to be used by WebScraper
     */
    public function __construct(HttpClientInterface $client, Crawler $crawler)
    {
        $this->httpClient = $client;
        $this->domCrawler = $crawler;
    }

    /**
     * Scrape web page for specified content.
     *
     * @return array scraped content
     */
    public function scrape(): array
    {
        try {
            $responce = $this->httpClient->request('GET', static::URL, ['verify' => false]);

            $this->domCrawler->clear();

            $this->domCrawler->addContent((string) $responce->getBody());

            return $this->parse();
        } catch (\Exception $exception) {
            throw new NoResultException('Can not process expected data', 0, $exception);
        }
    }

    /**
     * Parse content of a webpage to extract only necessary data.
     *
     * @return array parsed content
     */
    abstract protected function parse(): array;
}
