<?php

namespace LotteryScraper\Scraper;

use LotteryScraper\Exception\NoResultException;
use Symfony\Component\DomCrawler\Crawler;

class LottoScraper extends WebScraper
{
    const URL = 'https://www.lotto.pl/lotto/wyniki-i-wygrane';

    protected function parse(): array
    {
        $result = [];

        $this->domCrawler
            ->filterXPath("//tbody/tr[contains(@class, 'wynik')][position() <=3]")
            ->each(function (Crawler $crawler) use (&$result) {
                $gameName = $crawler
                    ->filterXPath('//td[1]/img/@alt')
                    ->text()
                ;

                $drawId = $crawler
                    ->filterXPath('//td[2]')
                    ->text()
                ;

                $drawDate = $crawler
                    ->filterXPath('//td[3]')
                    ->text()
                ;

                $standardNumbers = $crawler
                    ->filterXPath("//td[4]/div[((contains(@class, 'lotto') or contains(@class, 'lottoPlus')) and contains(@class, 'sortkolejnosc')) or contains(@class, 'lottoSzansa')]//span")
                    ->extract(['_text'])
                ;

                if (empty($gameName) || empty($standardNumbers) || empty($drawDate) || empty($drawId)) {
                    throw new NoResultException('No expected data found');
                }

                $result[$gameName] = [
                    'drawId' => $drawId,
                    'drawDate' => $drawDate,
                    'standardNumbers' => $standardNumbers,
                ];
            })
        ;

        if (empty($result)) {
            throw new NoResultException('No expected data found');
        }

        return $result;
    }
}
