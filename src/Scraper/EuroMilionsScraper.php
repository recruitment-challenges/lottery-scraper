<?php

namespace LotteryScraper\Scraper;

class EuroMilionsScraper extends WebScraper
{
    const URL = 'https://www.elgordo.com/results/euromillonariaen.asp';

    protected function parse(): array
    {
        $result['standardNumbers'] = $this->domCrawler
            ->filterXPath("//div[contains(@class, 'balls')]//div[contains(@class, 'num')]//span")
            ->extract(['_text']);

        $result['extraNumbers'] = $this->domCrawler
            ->filterXPath("//div[contains(@class, 'balls')]//div[contains(@class, 'esp')]//span[contains(@class, 'int-num')]")
            ->extract(['_text']);

        $result['drawDate'] = $this->domCrawler
            ->filterXPath("//div[contains(@class, 'balls')]")
            ->siblings()
            ->first()
            ->text();

        return ['EuroMilions' => $result];
    }
}
