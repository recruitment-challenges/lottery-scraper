<?php

namespace LotteryScraper\Scraper;

class EurojackpotScraper extends WebScraper
{
    const URL = 'https://www.lotto.pl/eurojackpot/wyniki-i-wygrane';

    protected function parse(): array
    {
        $result['standardNumbers'] = $this->domCrawler
            ->filterXPath("//tr[contains(@class, 'wynik')][1]//div[contains(@class, 'euroJackpot') and contains(@class, 'sortkolejnosc')]//div[contains(@class, 'advantageNumber')]/preceding-sibling::div/span")
            ->extract(['_text'])
        ;

        $result['extraNumbers'] = $this->domCrawler
            ->filterXPath("//tr[contains(@class, 'wynik')][1]//div[contains(@class, 'euroJackpot') and contains(@class, 'sortkolejnosc')]//div[contains(@class, 'advantageNumber')]/following-sibling::div/span")
                ->extract(['_text'])
        ;

        $result['drawId'] = $this->domCrawler
            ->filterXPath("//tr[contains(@class, 'wynik')][1]/td[1]")
            ->text()
        ;

        $result['drawDate'] = $this->domCrawler
            ->filterXPath("//tr[contains(@class, 'wynik')][1]/td[2]")
            ->text()
        ;

        return ['Eurojackpot' => $result];
    }
}
