<?php

namespace LotteryScraper\Command;

use GuzzleHttp\ClientInterface as HttpClientInterface;
use LotteryScraper\Scraper\EurojackpotScraper;
use LotteryScraper\Scraper\EuroMilionsScraper;
use LotteryScraper\Scraper\LottoScraper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;

class ScrapeCommand extends Command
{
    /**
     * @var array list of scrapers to be used by the command
     */
    const SCRAPERS = [EurojackpotScraper::class, EuroMilionsScraper::class, LottoScraper::class];

    /**
     * @var HttpClientInterface http client to be used by the command
     */
    private $httpClient;

    /**
     * @var array scraped data
     */
    private $data = [];

    /**
     * @param HttpClientInterface $httpClient client for performing http requests
     */
    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('scrape')
            ->setDescription('Scrape lottery results')
            ->setHelp('Scrape lottery results from defined websites');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->collectData()->saveToJSON();

            $output->writeln('Scraping lottery results sucessfuly completed!');
        } catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
        }
    }

    /**
     * Collect data from selected scrapers.
     *
     * @return self
     */
    protected function collectData(): self
    {
        foreach (static::SCRAPERS as $scraperClass) {
            $scraper = new $scraperClass(
                $this->httpClient,
                new Crawler()
            );

            $this->data += $scraper->scrape();
        }

        return $this;
    }

    /**
     * Saves data to specified file.
     *
     * @return self
     */
    protected function saveToJSON(): self
    {
        $jsonString = json_encode($this->data);
        $file = sprintf('%s//lotery_result.json', getcwd());

        if (false === file_put_contents($file, $jsonString)) {
            throw new \Exception(sprintf('Can not save report to %s', $file));
        }

        return $this;
    }
}
