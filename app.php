<?php

require_once 'vendor/autoload.php';

use GuzzleHttp\Client;
use LotteryScraper\Command\ScrapeCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$command = $application->add(
    new ScrapeCommand(new Client())
);

$application
    ->setDefaultCommand($command->getName(), 1)
    ->run()
;
